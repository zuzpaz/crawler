**Crawler**

---

## Program description

The program reads data from online board games shops. It separates information for individual games and then updates a database.

Used technologies:

- Beautiful Soup 
- requests 
- MongoDB

---
