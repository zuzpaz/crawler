import requests
from bs4 import BeautifulSoup
import open_collection


def parse_upsert_game(url):
    """Parse one game from Gamiko and upsert to database in MongoDB."""
    game = {}
    game_site = 'gamiko'
    doc_html = requests.get(url)
    doc_html.encoding = "utf-8"
    soup = BeautifulSoup(doc_html.content, 'html.parser')
    row = soup.body.main.section.find(itemtype='https://schema.org/Product')
    game['image_url'] = row.find('img')['data-image-large-src']

    game['title'] = row.find(itemprop='name').text

    open_collection.update_game(game)  # update game in mongo

    if row.find('button').has_attr('disabled'):
        availability = 'Out of Stock'
    else:
        availability = 'In Stock'
    price = float(row.find(itemprop='price')['content'])

    open_collection.update_game_site(game['title'], game_site, price, availability, url)  # update game's site in mongo


def open_page(url, start, stop):
    """Open pages from 'start' to 'stop' number."""
    if start <= stop <= 10:
        for i in range(start, stop + 1):
            doc_html = requests.get(url + '&page=' + str(i))
            soup = BeautifulSoup(doc_html.text, 'html.parser')
            products_row = soup.section.find(id='js-product-list').div.find_all('article')
            for article in products_row:
                game_url = article.div.div.a['href']
                parse_upsert_game(game_url)
        print('Updated')

    else:
        print('Not valid page numbers')


open_page('https://gamiko.pl/pl/57-gry-planszowe-i-towarzyskie?order=product.name.asc', 1, 1)
# open_collection.update_game(parse_upsert_game('https://gamiko.pl/pl/imprezowe-dedukcyjne-wieloosobowe/270-10minut-3770004610006.html'))
# parse_upsert_game('https://gamiko.pl/pl/imprezowe-dedukcyjne-wieloosobowe/270-10minut-3770004610006.html')
