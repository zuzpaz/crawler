from pymongo import MongoClient

collection = MongoClient('mongodb://localhost:27017/')['boardgames']['games_collection']


def update_game(game):
    """Update game document and create description about site (price, availability etc.) if none exist."""
    collection.update_one({'title': game['title']}, {'$set': game, '$setOnInsert': {'site': []}}, upsert=True)
    # setOnInsert is an oparation only on new created document, not on updated. If exist document setOnInsert has no effect


def update_game_site(game_title, game_site, price, availability, url):
    """Update description about one page without overwriting existed another site."""
    try:
        collection.update_one({'title': game_title, "site": {'$elemMatch': {"name": game_site}}},
                              {'$set': {"site.$.price": price, "site.$.availability": availability, "site.$.url": url}},
                              upsert=True)
    except:
        collection.update_one({'title': game_title}, {'$addToSet': {'site': {'name': game_site, 'price': price,
                                                                             'availability': availability}}})

