import requests
from bs4 import BeautifulSoup
import open_collection


def parse_upsert_game(url):
    """Parse one game from AM76 and upsert to database in MongoDB."""
    game = {}
    game_site = 'am76'
    doc_html = requests.get(url)
    doc_html.encoding = "utf-8"
    soup = BeautifulSoup(doc_html.content, 'html.parser')
    colsm10 = soup.find(itemtype='http://schema.org/Product')
    game['image_url'] = colsm10.find(id='produkt_naglowek').div.a['href']

    game['title'] = colsm10.div.h1.text.upper()  # to unify titles from all pages

    tab = soup.find_all('div', 'tab-content')[0].div.dl

    categories = tab.find('dt', string='Kategorie sklep').findNext('dd').find_all('h3')
    game['category'] = [c.contents[0].lower() for c in categories]

    bgg = tab.find('dt', string='Pozycja na BGG')
    if bgg:
        game['BGG rank'] = int(bgg.findNext('dd').find('a').contents[0])
        game['BGG link'] = bgg.findNext('dd').a['href']

    open_collection.update_game(game)  # update game in mongo

    if colsm10.find(itemprop='availability')['content'] == 'InStock':
        availability = 'In Stock'
    else:
        availability = 'Out of Stock'

    price = float(colsm10.find(itemprop='offers').find_all('span', 'price')[0].text)

    open_collection.update_game_site(game['title'], game_site, price, availability, url)  # update game's site in mongo


def open_page(url, start, stop):
    """Open pages from 'start' to 'stop' number."""
    if start <= stop <= 10:
        for i in range(start, stop + 1):
            doc_html = requests.get(url + '?&sort=nazwa&p=' + str(i))
            soup = BeautifulSoup(doc_html.text, 'html.parser')
            rows = soup.section.div.find(itemtype='http://schema.org/Product').find_all('div', 'row')
            for r in rows:
                for c in r.find_all('div', 'panel-body'):
                    game_url = c.a['href']
                    parse_upsert_game('https://am76.pl/' + game_url)
        print('Updated')

    else:
        print('Not valid page numbers')

open_page('https://am76.pl/dzial,gry-planszowe:1', 1, 1)
# open_collection.update_game(parse_upsert_game('https://am76.pl/produkt,10-minut:3326'))
# parse_upsert_game('https://am76.pl/produkt,10-minut:3326')
